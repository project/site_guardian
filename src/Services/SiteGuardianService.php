<?php

namespace Drupal\site_guardian\Services;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandler;
use Drupal\Core\KeyValueStore\KeyValueExpirableFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Utility\ProjectInfo;
use Drupal\update\ProjectCoreCompatibility;
use Drupal\update\UpdateFetcherInterface;
use Drupal\update\UpdateProcessorInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides Site Guardian service helper functions.
 *
 * @group site_guardian
 */
class SiteGuardianService {

  use StringTranslationTrait;

  /**
   * Settings for Site Guardian.
   */
  protected Config $settings;

  /**
   * The logger channel.
   */
  protected LoggerInterface $logger;

  /**
   * The module handler service.
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The module list service.
   */
  protected ModuleExtensionList $moduleList;

  /**
   * The theme handler.
   */
  protected ThemeHandler $themeHandler;

  /**
   * The keyvalue expirable factory.
   */
  protected KeyValueExpirableFactoryInterface $keyValueExpirableFactory;

  /**
   * Update Processor Service.
   */
  protected UpdateProcessorInterface $updateProcessor;

  /**
   * The state key value store.
   */
  protected StateInterface $state;

  /**
   * Constructs a SiteGuardianService object.
   */
  public function __construct(ConfigFactoryInterface $config, LoggerInterface $logger, ModuleHandlerInterface $module_handler, ModuleExtensionList $extension_list_module, ThemeHandler $theme_handler, KeyValueExpirableFactoryInterface $key_value_expirable_factory, UpdateProcessorInterface $update_processor, StateInterface $state, TranslationInterface $string_translation) {
    $this->settings = $config->getEditable('site_guardian.settings');
    $this->logger = $logger;
    $this->moduleHandler = $module_handler;
    $this->moduleList = $extension_list_module;
    $this->themeHandler = $theme_handler;
    $this->keyValueExpirableFactory = $key_value_expirable_factory;
    $this->updateProcessor = $update_processor;
    $this->state = $state;
    $this->stringTranslation = $string_translation;
  }

  /**
   * Gets the Site Guardian Key from settings.
   *
   * @return string
   *   Returns a Site Guardian Key string.
   */
  public function getKey(): string {
    return $this->settings->get('site_guardian_key');
  }

  /**
   * {@inheritdoc}
   */
  public function generateKey(): void {
    $new_site_guardian_key = Crypt::randomBytesBase64(55);
    $this->settings->set('site_guardian_key', $new_site_guardian_key)
      ->save();
  }

  /**
   * Checks whether Site Guardian is activated or not.
   */
  public function isActivated(): bool {
    return boolval($this->settings->get('site_guardian_activated'));
  }

  /**
   * Gets notes to do with the site.
   */
  public function getSiteNotes(): string {
    return $this->settings->get('site_guardian_notes');
  }

  /**
   * Allows access to API or not.
   *
   * @return bool
   *   Returns whether access is allowed.
   */
  public function accessAllowed(string $site_guardian_key): bool {
    if (!$this->isActivated()) {
      $this->logger->warning("Site Guardian request received but Site Guardian API has not been activated.");
      return FALSE;
    }
    $saved_key = $this->getKey();
    if (empty($saved_key) || empty($site_guardian_key) || !hash_equals($site_guardian_key, $saved_key)) {
      $this->logger->warning("Site Guardian request received with an incorrect key, access was denied.");
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Core functionality of module - to check for modules and updates.
   *
   * @return mixed[]
   *   Returns an array of enabled modules along with their update statuses.
   */
  public function getEnabledModulesAndUpdates(): array {

    $projects = [];
    $this->moduleHandler->loadInclude('update', 'inc', 'update.compare');

    // First, get a list of projects. This part borrows code from get_projects()
    // in UpdateManager.php, before including modules created using the
    // Features module, as well as custom modules/themes.
    $modules = $this->moduleList->reset()->getList();
    // Change.
    $themes = $this->themeHandler->rebuildThemeData();

    foreach ($modules as $module_name => $module_data) {
      if (preg_match('(modules/features)', $module_data->subpath)) {
        $module_data->info['project'] = $module_name . '_created_by_features_module';
      }
      if ($module_data->origin != 'core' && $module_data->status == '1' && !isset($module_data->info['project'])) {
        $module_data->info['project'] = $module_name;
      }
    }

    foreach ($themes as $theme_name => $theme_data) {
      if (preg_match('(themes/features)', $theme_data->subpath)) {
        $theme_data->info['project'] = $theme_name . '_created_by_features_module';
      }
      if ($theme_data->origin != 'core' && $theme_data->status == '1' && !isset($theme_data->info['project'])) {
        $theme_data->info['project'] = $theme_name;
      }
    }

    $project_info = new ProjectInfo();

    $project_info->processInfoList($projects, $modules, 'module', TRUE);
    $project_info->processInfoList($projects, $themes, 'theme', TRUE);

    $this->moduleHandler->alter('update_projects', $projects);
    // End of code borrowed from get_projects().
    //
    // Now try to get available update data. This marks the start of amended
    // code borrowed from update_get_available in update.module.
    $needs_refresh = FALSE;
    $available = $this->keyValueExpirableFactory->get('update_available_releases')->getAll();

    foreach ($projects as $key => $project) {

      if (empty($available[$key])) {
        $this->updateProcessor->createFetchTask($project);
        $needs_refresh = TRUE;
        continue;
      }
      if ($project['info']['_info_file_ctime'] > $available[$key]['last_fetch']) {
        $available[$key]['fetch_status'] = UpdateFetcherInterface::FETCH_PENDING;
      }
      if (empty($available[$key]['releases']) && !$available[$key]['last_fetch']) {
        $available[$key]['fetch_status'] = UpdateFetcherInterface::FETCH_PENDING;
      }
      if (!empty($available[$key]['fetch_status']) && $available[$key]['fetch_status'] == UpdateFetcherInterface::FETCH_PENDING) {
        $this->updateProcessor->createFetchTask($project);
        $needs_refresh = TRUE;
      }
    }

    if ($needs_refresh) {
      update_fetch_data();
      $available = $this->keyValueExpirableFactory->get('update_available_releases')->getAll();
    }
    // End of borrowing from update_get_available.
    //
    // Next, compare list of projects with available update data. This section
    // draws from update_calculate_project_data in update.compare.inc.
    update_process_project_info($projects);

    if (isset($projects['drupal']) && !empty($available['drupal'])) {
      update_calculate_project_update_status($projects['drupal'], $available['drupal']);
      if (isset($available['drupal']['releases']) && !empty($available['drupal']['supported_branches'])) {
        $supported_branches = explode(',', $available['drupal']['supported_branches']);
        $project_core_compatibility = new ProjectCoreCompatibility($projects['drupal'], $available['drupal']['releases'], $supported_branches);
      }
    }

    foreach ($projects as $project => $project_info) {

      if (isset($available[$project])) {
        if ($project === 'drupal') {
          continue;
        }
        update_calculate_project_update_status($projects[$project], $available[$project]);
        if (isset($project_core_compatibility)) {
          $project_core_compatibility->setReleaseMessage($projects[$project]);
        }
      }
      else {
        $projects[$project]['status'] = UpdateFetcherInterface::UNKNOWN;
        $projects[$project]['reason'] = $this->t('No available releases found');
      }
    }

    $this->moduleHandler->alter('update_status', $projects);
    // End of code adapted from update_calculate_project_data.
    //
    // Finally, add a datestamp for when these checks were last performed,
    // if ever.
    $projects['last_checked'] = $this->state->get('update.last_check') ?: 0;

    return $projects;
  }

}
