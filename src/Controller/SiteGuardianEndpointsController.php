<?php

namespace Drupal\site_guardian\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\Core\Url;
use Drupal\site_guardian\Services\SiteGuardianService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides route responses for the Site Guardian module.
 *
 * @group site_guardian
 */
class SiteGuardianEndpointsController extends ControllerBase {

  /**
   * The database connection.
   */
  protected Connection $database;

  /**
   * The route provider.
   */
  protected RouteProviderInterface $routeProvider;

  /**
   * Site Guardian service.
   */
  protected SiteGuardianService $siteGuardianService;

  public function __construct(
    Connection $database,
    RouteProviderInterface $route_provider,
    SiteGuardianService $siteGuardianService
  ) {
    $this->database = $database;
    $this->routeProvider = $route_provider;
    $this->siteGuardianService = $siteGuardianService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SiteGuardianEndpointsController {

    $database = $container->get('database');
    $routeProvider = $container->get('router.route_provider');
    $siteGuardianService = $container->get('site_guardian.SiteGuardianService');

    return new static($database, $routeProvider, $siteGuardianService);
  }

  /**
   * Gets a list of endpoints provided by the module.
   *
   * @return mixed[]
   *   An array of available endpoints.
   */
  public function endpoints(): array {

    // Get the API key.
    $siteGuardianKey = $this->siteGuardianService->getKey();

    // Get all routes defined by this module.
    // Get from DB directly rather than from routing.yml in case any routes are
    // ever modified/programatically added.
    $query = $this->database->query("SELECT name, path FROM {router} WHERE name LIKE :name", [":name" => 'site_guardian.endpoint.%']);
    $results = $query->fetchAll();

    $build = [];

    // Now build the output.
    foreach ($results as $result) {
      $route = $this->routeProvider->getRouteByName($result->name);
      $title = $route->getDefault('_title');
      $description = $route->getDefault('_description');
      $link = Link::fromTextAndUrl($title, Url::fromRoute($result->name, ['site_guardian_key' => $siteGuardianKey]));

      $build[] = [
        '#markup' => '<p>' . $link->toString() . '<br>' . $description . '</p>',
      ];
    }

    $build['#cache'] = [
      'tags' => ['config:site_guardian.settings'],
    ];

    return $build;
  }

}
