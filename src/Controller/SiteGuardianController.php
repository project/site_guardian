<?php

namespace Drupal\site_guardian\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Flood\FloodInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\site_guardian\Services\SiteGuardianService;
use Drupal\system\SystemManager;
use Drupal\update\UpdateManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides route responses for the Site Guardian module.
 *
 * @group site_guardian
 */
class SiteGuardianController extends ControllerBase {

  /**
   * The flood service.
   */
  protected FloodInterface $flood;

  /**
   * The logger channel.
   */
  protected LoggerInterface $logger;

  /**
   * The request stack.
   */
  protected RequestStack $requestStack;

  /**
   * Site Guardian object.
   */
  protected SiteGuardianService $siteGuardian;

  /**
   * Site Guardian Key string.
   */
  protected string $siteGuardianKey;

  /**
   * System Manager Service.
   */
  protected SystemManager $systemManager;

  /**
   * Update manager service.
   */
  protected UpdateManagerInterface $updateManager;

  public function __construct(
    FloodInterface $flood,
    LoggerInterface $logger,
    RequestStack $request_stack,
    SiteGuardianService $site_guardian,
    string $site_guardian_key,
    SystemManager $systemManager,
    UpdateManagerInterface $update_manager
  ) {
    $this->flood = $flood;
    $this->logger = $logger;
    $this->requestStack = $request_stack;
    $this->siteGuardian = $site_guardian;
    $this->siteGuardianKey = $site_guardian_key;
    $this->systemManager = $systemManager;
    $this->updateManager = $update_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): SiteGuardianController {

    $flood = $container->get('flood');
    $logger = $container->get('logger.channel.site_guardian');
    $request_stack = $container->get('request_stack');
    $site_guardian = $container->get('site_guardian.SiteGuardianService');
    $site_guardian_key = $request_stack->getCurrentRequest()->query->get('site_guardian_key');
    $system_manager = $container->get('system.manager');
    $update_manager = $container->get('update.manager');

    return new static($flood, $logger, $request_stack, $site_guardian, $site_guardian_key, $system_manager, $update_manager);
  }

  /**
   * Outputs JSON list of version info for Drupal, PHP and DB (amongst others).
   */
  public function getSiteStatusReport(): JsonResponse {

    $this->logger->notice("Received request for status report.");
    $status_report_info = $this->systemManager->listRequirements();

    // Get any enhanced status info from any modules implementing hook_site_guardian_status
    $new_reports = \Drupal::moduleHandler()->invokeAll('site_guardian_status');

    // Merge any enhanced status into the report info 
    if (!empty($new_reports)) {
      $status_report_info = array_merge($status_report_info, $new_reports);
    }

    // Return the response
    return new JsonResponse($status_report_info);
  }

  /**
   * Outputs JSON list of enabled projects, versions and recommended updates.
   */
  public function listEnabledModulesAndUpdates(): JsonResponse {

    $this->logger->notice("Received request for enabled modules and updates.");
    $projects = $this->siteGuardian->getEnabledModulesAndUpdates();

    return new JsonResponse($projects);
  }

  /**
   * Checks access for a specific request.
   */
  public function access(AccountInterface $account): AccessResultInterface {

    // If access is allowed.
    if ($this->siteGuardian->accessAllowed($this->siteGuardianKey)) {

      // Clear flood prevention check if required.
      if (!$this->flood->isAllowed('site_guardian_check_attempt', 10)) {
        $this->flood->clear('site_guardian_check_attempt');
      }

      return AccessResult::allowed();
    }

    // If access is denied.
    else {

      // Register the flood event for invalid attempts.
      $this->flood->register('site_guardian_check_attempt');

      // Use the flood api to prevent brute force attacks. Limits access by IP
      // address to 10 attempts per hour.
      // If flood protection has been triggered for the requests ip address
      // then log it.
      if (!$this->flood->isAllowed('site_guardian_check_attempt', 10)) {

        $ip = $this->requestStack->getCurrentRequest()->getClientIp();

        $this->logger->warning('Flood prevention has been triggered. It is possible that an attacker at @ip is targeting your site. If the problem persists, consider banning that IP address.', ['@ip' => $ip]);
      }

      return AccessResult::forbidden();
    }
  }

}
