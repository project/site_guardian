<?php

namespace Drupal\site_guardian\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\site_guardian\Services\SiteGuardianService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Config form class.
 *
 * @group site_guardian
 */
class SiteGuardianForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'site_guardian.settings';

  /**
   * Site Guardian object.
   */
  protected object $siteGuardian;

  /**
   * @param SiteGuardianService $site_guardian
   *   The Site Guardian service.
   * @param MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(SiteGuardianService $site_guardian, MessengerInterface $messenger) {
    $this->siteGuardian = $site_guardian;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('site_guardian.SiteGuardianService'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_guardian_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $settings = $this->config(static::SETTINGS);

    $form['site_guardian_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Guardian Key'),
      '#default_value' => $settings->get('site_guardian_key'),
      '#description' => $this->t("The Site Guardian Key prevents the Site Guardian API from being called without permission. It can be changed here but cannot be left blank.
                        <br><br>The key is set randomly upon enabling the module - whether or not it is changed here, it must be matched by the key anywhere a Site Guardian endpoint is called (such as in the Site Guardian Client) for access to be granted.
                        <br><br>Note that if the Site Guardian module is disabled, re-enabling it will reset the Site Guardian Key to its default value, namely a new random key."),
    ];

    $form['site_guardian_key_generate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate new Site Guardian Key'),
      '#description' => $this->t("Please note that clicking this button will erase any existing Site Guardian Key before generating and saving a random new one."),
      '#submit' => ['::generateKey'],
    ];

    $form['site_guardian_notes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Site Notes'),
      '#default_value' => $settings->get('site_guardian_notes'),
      '#description' => $this->t("Here you can provide short notes regarding your site that can be passed to and viewed in any application consuming data from a Site Guardian endpoint (such as in the Site Guardian Client) - for instance, details of patches applied, or any special considerations, etc."),
    ];

    $form['site_guardian_activated'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Activate Site Guardian'),
      '#default_value' => (int) boolval($settings->get('site_guardian_activated')),
      '#description' => $this->t("Upon enabling the Site Guardian module, this box is checked by default, and therefore the module is running straight away.
                        <br><br>Uncheck this box and hit save to deactivate Site Guardian's central functionality without requiring the module to be disabled entirely."),
    ];

    $form['site_guardian_endpoints'] = [
      '#title' => $this
        ->t('Click here to see endpoints exposed by Site Guardian'),
      '#type' => 'link',
      '#url' => Url::fromRoute('site_guardian.endpoints'),
      '#states' => [
        'visible' => [
          ':input[name="site_guardian_activated[value]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $triggering_element_parent = $form_state->getTriggeringElement()['#parents'][0];

    if ($triggering_element_parent != 'site_guardian_key_generate') {
      if (strlen($form_state->getValue('site_guardian_key')) < 1) {
        $form_state->setErrorByName('site_guardian_key', $this->t(
          'The Site Guardian Key is a required field. You can use the button below to generate and save a random new one, but please do not leave the field empty.'
        ));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function generateKey(array &$form, FormStateInterface $form_state): void {
    $this->siteGuardian->generateKey();
    $this->messenger->addStatus($this->t('A new Site Guardian Key has been generated and saved.'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Need editable settings here in order to save.
    $settings = $this->configFactory->getEditable(static::SETTINGS);

    $settings->set('site_guardian_key', $form_state->getValue('site_guardian_key'))
      ->set('site_guardian_activated', (int) boolval($form_state->getValue('site_guardian_activated')))
      ->set('site_guardian_notes', $form_state->getValue('site_guardian_notes'));

    $settings->save();

    return parent::submitForm($form, $form_state);
  }

}
