<?php

namespace Drupal\Tests\site_guardian\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\site_guardian\Services\SiteGuardianService;
use Drupal\Tests\UnitTestCase;
use object\Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Simple test to ensure that asserts pass.
 *
 * @group site_guardian
 */
class SiteGuardianServiceUnitTest extends UnitTestCase {

  const SITE_GUARDIAN_KEY = 'MQ-pftkwarcOKu9h3FhQdtkrN1c8bCvRsUeagWAxqW1zV6V3UCWrsQJv2s6P2EiTFB-WkR7V3A';

  /**
   * The configuration Object.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * {@inheritdoc}
   */
  public function setUp() {

    parent::setUp();

    $config_map = [
      'site_guardian.settings' => [
        'site_guardian_key' => self::SITE_GUARDIAN_KEY,
        'site_guardian_activated' => 1,
      ],
    ];

    // Get a stub for the config.factory service.
    $this->configFactory = $this->getConfigFactoryStub($config_map);
    $container = new ContainerBuilder();
    // Set the config.factory in the container also.
    $container->set('config.factory', $this->configFactory);

    \Drupal::setContainer($container);
  }

  /**
   * @covers Drupal\site_guardian\Services\SiteGuardianService::getKey
   */
  public function testGetKey(): void {

    $this->site_guardian = new SiteGuardianService($this->configFactory);
    $this->assertEquals(self::SITE_GUARDIAN_KEY, $this->site_guardian->getKey());
  }

  /**
   * @covers Drupal\site_guardian\Services\SiteGuardianService::isActivated
   */
  public function testIsActivated(): void {

    $this->site_guardian = new SiteGuardianService($this->configFactory);
    $this->assertEquals(1, $this->site_guardian->isActivated());
  }

  /**
   * @covers Drupal\site_guardian\Services\SiteGuardianService::checkActivationAndSiteGuardianKey
   */
  public function testCheckActivationAndSiteGuardianKey(): void {

    $this->site_guardian = new SiteGuardianService($this->configFactory);
    $this->assertEquals('Checks passed', $this->site_guardian->checkActivationAndSiteGuardianKey(self::SITE_GUARDIAN_KEY));
    $this->assertEquals('Access denied', $this->site_guardian->checkActivationAndSiteGuardianKey('BAD KEY'));
  }

  /**
   * @covers Drupal\site_guardian\Services\SiteGuardianService::accessAllowed
   */
  public function testAccessAllowed(): void {

    $this->site_guardian = new SiteGuardianService($this->configFactory);
    $this->assertEquals(TRUE, $this->site_guardian->accessAllowed(self::SITE_GUARDIAN_KEY));
    $this->assertEquals(FALSE, $this->site_guardian->accessAllowed('BAD KEY'));
  }

  /**
   * @covers Drupal\site_guardian\Services\SiteGuardianService::getEnabledModulesAndUpdates
   */
  public function testGetEnabledModulesAndUpdates(): void {

    $this->site_guardian = new SiteGuardianService($this->configFactory);
    $this->site_guardian->getEnabledModulesAndUpdates();

  }

}
