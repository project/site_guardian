# Site Guardian

The Site Guardian module securely exposes information relating to your site's
core, modules, versions, updates, etc., in JSON so that it can be consumed
elsewhere. That allows you to more easily monitor all the sites that you
maintain without having to log in to each of them individually.

## Keeping your site secure and up-to-date

Drupal has functionality that helps you monitor the state of your website:

   * The “Available updates” page (at ``/admin/reports/updates``) lists all
modules currently enabled, their version and the recommended version, as well as
security status. You need to log in to view this.
   * The “Status Report” page (at ``/admin/reports/status``) shows things like
the current version of PHP and database engine. You need to log in to view this.
   * An email alert can be configured (via ``/admin/reports/updates/settings``)
to alert you that “things are out of date”. You need to log in to check exactly
which things.

The Drupal.org security working group do an amazing job and regularly send out
emails (to those subscribed) detailing any new security updates that are
available for core and contrib. This list is also available via an RSS feed at
https://www.drupal.org/security/rss.xml.

When a core security fix is released, it's obvious you need to update core, but
for contrib vulnerabilities, you need to log in to every single site you look
after to see whether they use that module. That takes a huge amount of time when
you're looking after more than a few websites. Each will have their own access
credentials (ideally protected by TFA, people!) and perhaps maintained by
different teams. You could easily miss an important security update and thus
leave a site vulnerable.

More generally, you should regularly update core and contrib to the latest
versions available. That means you're using the latest code, but also means that
if/when a security issue is released, it's probably just a minor update to what
you already have. If you are jumping from version 1.2 of a module to a security
fix at version 1.34, that is a much bigger risk than updating from version 1.33.

## What Site Guardian does

The Site Guardian module allows you to securely grab all the info about your
site in JSON format so it can be used elsewhere. The endpoints can only be hit
if you use the right Site Guardian Key, set up by you beforehand in the module
on each individual website.

Having this data accessible from outside the websites makes it quicker to check
for security vulnerabilities and keep them up-to-date.

## What you can monitor

The ready availability of this info means you can see:

* An overview of all sites monitored
* The current version of Drupal being used
* What version of PHP is being used, to plan for any EOL dates in advance
* Any outstanding Drupal core or contrib security issues
* That modules are being kept up-to-date
* Which sites are using a particular module when a new security advisory is
released, so we can create and deploy a hot fix.

Giving your whole team access to such info for potentially dozens or even
hundreds of sites is a massive time-saving and means you can easily keep your or
your clients' websites up-to-date and secure.

## Similar modules
###  Vitals and Vitals Extras modules.
Vitals and V Extra makes quite a few assumptions and recommended certain
modules. While we didn't disagree with those recommendations, we didn't want
them to be required on every site and not something we thought should be
reported.
We hope to keep this module very lightweight and only show things Drupal is
already reporting, but making available externally.
As an example Vitals only reports the modules requiring an update, but Drupal
can show all the modules installed.
Our upstream server gathering the data will be making recommendations and
highlighting issues rather than the site itself.

### drd_agent
This also exposes an endpoint to allow information to be gathered by a third
party source.
It seemed to take a different approach to both Site Guardian and Vitals,
however, in that it reports very different information than we wanted.

### Conclusion
The decision was made to create another module based on the direction that we
hope to take with this module.
Site Guardian seems to fit right in the seam between drd_agent and vitals, where
it is doing some similar things to both but in slightly different ways.

## Tests

To run these tests, execute the following command from web root (htdocs):
`phpunit -c core/phpunit.xml.dist modules/custom/site_guardian/tests/src/Unit`

## Configuration management

When exporting the site configuration, the site_guardian_key will be included
in the config file with everything else.
This is not unusual, as most modules use the configs to store private keys,
passwords, etc., but there are any number of reasons why you may not wish to
store the exported value and there are several ways to avoid this.
You may try overwriting the value in the settings.php for example. You could
also consider the [State API](https://www.drupal.org/docs/8/api/state-api/overview)
or the [Config ignore module](https://www.drupal.org/project/config_ignore)
to help with this.
